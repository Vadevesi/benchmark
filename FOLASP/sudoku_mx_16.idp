// "Naive" sudoku implementation in IDP3
// Simply generates a grid according to sudoku rules
// Author: Simon Vandevelde (s.vandevelde@kuleuven.be)
vocabulary V {
    type Row isa int
    type Col isa int
    type Value isa int

    cell_value(Row, Col): Value
    same_box(Row, Col, Row, Col)
}

theory T:V{
    !r[Row]: !c1[Row]: !c2[Col]: c1 ~= c2 => cell_value(r, c1) ~= cell_value(r, c2).

    !c[Col]: !r1[Row]: !r2[Row]: r1 ~= r2 => cell_value(r1, c) ~= cell_value(r2, c).

    !r1[Row]: !r2[Row]: !c1[Col]: !c2[Col]: same_box(r1, c1, r2, c2) => cell_value(r1, c1) ~= cell_value(r2, c2).

    {
    !r1[Row]: !r2[Row]: !c1[Col]: !c2[Col]: same_box(r1, c1, r2, c2) <- ~(r1 = r2 & c1 = c2) & (r1 - r1%3)/3 = (r2 - r2%3)/3 & (c1 - c1%3)/3 = (c2 - c2%3)/3.
    }
}

structure S:V{
    Row = {0..15}
    Col = {0..15}
    Value = {1..16}
    
}

procedure main() {
    printmodels(modelexpand(T,S))
}
