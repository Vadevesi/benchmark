# Generates a structure of variable size for the IDP-Z3 propagation benchmark.
# Also generates the ASP equivalent.

import random

random.seed(2023)
for size in range(0, 110, 10):

    struct = 'structure {\n'
    struct += f'Type1 := {{1..{size}}}.\n'
    struct += f'Type2 := {{1..{size*5}}}.\n'
    struct += f'mean := {size//2}.\n'

    asp = f'type1(1..{size}).\n'
    asp += f'type2(1..{size*5}).\n'
    asp += f'mean({size//2}).\n'

    function_vals = []
    asp_vals = []
    for i in range(1, size*5+1):
        choice = random.randint(1, size)
        function_vals.append(f'{i} -> {choice}')
        asp_vals.append(f'{i}, {choice}')
    struct += f"function1 := {{{', '.join(function_vals)}}}.\n"
    struct += '}\n'

    asp += f'function1({"; ".join(asp_vals)}).\n'

    with open('./stureject_theory.idp', 'r') as fp:
        theory = fp.read()
    with open(f'stureject_prop_{size}.idp', 'w') as fp:
        fp.write(struct)
        fp.write(theory)

    with open('./stureject_theory.lp', 'r') as fp:
        theory = fp.read()
    with open(f'stureject_prop_{size}.lp', 'w') as fp:
        fp.write(asp)
        fp.write('\n\n')
        fp.write(theory)
