// IDP-Z3 implementation of the graceful graphs example
// https://www.mat.unical.it/aspcomp2013/GracefulGraphs
// This is the FOLASP implementation by Kylian Van Dessel, adapted for IDP-Z3 by Simon Vandevelde (s.vandevelde@kuleuven.be).

vocabulary {
  type node := {0..8}
  type val := {0..20}
  edge: (node * node) -> Bool
  edge_value: (node * node) -> val
  value: node -> val
}

theory {
    // Each value should be used exactly once. (With an exception for 0, as 0 is also used when there is no edge).
    !V in val: V  > -1 => (#{X, Y in node: (edge_value(X,Y)  =  V) }  =  1).

    // Values should be distinct.
    ! N1, N2 in node: N1 ~= N2 => value(N1) ~= value(N2).
 

    {
      ! X, Y in node: edge_value(X, Y) = -1 <- (~edge(X, Y)).
      ! VAR4 in val: !X, Y in node: edge_value(X, Y) = VAR4 <- (edge(X, Y) & VAR4  = abs(value(X) - value(Y))).
    }
}

structure{
  edge := { 
(0,1), (0,2), (0,3), (0,4), (0,5), (0,6), (0,7), (0,8), (7,8), (6,8), (5,8), (6,7), (5,7), (5,6), (3,4), (2,4), (1,4), (2,3), (1,3), (1,2)
}.
}

procedure main(){
    pretty_print(model_expand(T,S,max=1,timeout_seconds=0))
}
