vocabulary {
    type Node := {1..11}
    type Val := {0..35}

    edge: Node * Node -> Bool
    edge_val: Node * Node -> Val
    node_val: Node -> Val
}

theory {
    // All node values must be different.
    !n1, n2 in Node: n1 ~= n2 => node_val(n1) ~= node_val(n2).

    // All edge values must be different, and each value must be used at least once.
    !n1, n2, n3, n4 in Node: n1 ~= n3 & n2 ~= n4 & edge(n1, n2) & edge(n3, n4) => edge_val(n1, n2) ~= edge_val(n3, n4).
    !v in Val: v ~= 0 => ?n1, n2 in Node: edge_val(n1, n2) = v.

    // Edge value is equal to the difference of the node's values (or, 0 in the case of no edge).
    !n1, n2 in Node: ~edge(n1, n2) => edge_val(n1, n2) = 0.
    !n1, n2 in Node: edge(n1, n2) & node_val(n1) > node_val(n2) => edge_val(n1, n2) = node_val(n1) - node_val(n2).
    !n1, n2 in Node: edge(n1, n2) & node_val(n1) < node_val(n2) => edge_val(n1, n2) = node_val(n2) - node_val(n1).
}

structure {
   //edge := {(a, b), (b, c), (c, a)}. 
    edge := {(7,1), (7,11), (6,11), (5,11), (4,11), (3,11), (2,11), (1,11), (7,10), (6,10), (5,10), (4,10), (3,10), (2,10), (1,10), (7,9), (6,9), (5,9), (4,9), (3,9), (2,9), (1,9), (7,8), (6,8), (5,8), (4,8), (3,8), (2,8), (1,8), (6,7), (5,6), (4,5), (3,4), (2,3), (1,2)}.
}
procedure main(){
    pretty_print(model_expand(T,S,max=1,timeout_seconds=0))
}
