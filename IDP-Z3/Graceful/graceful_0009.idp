// IDP-Z3 implementation of the graceful graphs example
// https://www.mat.unical.it/aspcomp2013/GracefulGraphs
// This is the FOLASP implementation by Kylian Van Dessel, adapted for IDP-Z3 by Simon Vandevelde (s.vandevelde@kuleuven.be).

vocabulary {
  type node := {1..12}
  type val := {0..36}
  edge: (node * node) -> Bool
  edge_value: (node * node) -> val
  value: node -> val
}

theory {
    // Each value should be used exactly once. (With an exception for 0, as 0 is also used when there is no edge).
    !V in val: V  >  0 => (#{X, Y in node: (edge_value(X,Y)  =  V) }  =  1).

    // Values should be distinct.
    ! N1, N2 in node: N1 ~= N2 => value(N1) ~= value(N2).
 

    {
      ! X, Y in node: edge_value(X, Y) = 0 <- (~edge(X, Y)).
      ! VAR4 in val: !X, Y in node: edge_value(X, Y) = VAR4 <- (edge(X, Y) & VAR4  = abs(value(X) - value(Y))).
    }
}

structure{
  edge := { 
(11,12), (10,12), (9,12), (8,12), (7,12), (10,11), (9,11), (8,11), (7,11), (9,10), (8,10), (7,10), (8,9), (7,9), (7,8), (5,6), (4,6), (3,6), (2,6), (1,6), (4,5), (3,5), (2,5), (1,5), (3,4), (2,4), (1,4), (2,3), (1,3), (1,2), (6,12), (5,11), (4,10), (3,9), (2,8), (1,7)
}.
}

procedure main(){
    pretty_print(model_expand(T,S,max=1,timeout_seconds=0))
}
