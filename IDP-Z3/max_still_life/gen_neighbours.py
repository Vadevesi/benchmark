neighbours = ["(x > 0 & y > 0 & gen1(x-1, y-1))",
              "(x > 0 & gen1(x-1, y))",
              "(x > 0 & y < 5 & gen1(x-1, y+1))",

              "(y > 0 & gen1(x, y-1))",
              "(y < 5 & gen1(x, y+1))",

              "(x < 5 & y > 0 & gen1(x+1, y-1))",
              "(x < 5 & gen1(x+1, y))",
              "(x < 5 & y < 5 & gen1(x+1, y+1)"]

nb_alive = 3
alive_constraints = []
for i in range(2**8):
    bin_string = format(i, '08b')
    if bin_string.count('1') != nb_alive:
        continue
    constraints = []
    for j, bit in enumerate(bin_string):
        if bit == '1':
            constraints.append(neighbours[j])
        else:
            constraints.append(f"~{neighbours[j]}")
            # print(f"~{neighbours[j]}")
    alive_constraints.append('(' + ' & '.join(constraints) + ')\n')
print(' | '.join(alive_constraints))
