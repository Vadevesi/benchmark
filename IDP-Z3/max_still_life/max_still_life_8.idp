// Specification to find a maximum density still life.
// Author: Simon Vandevelde (s.vandevelde@kuleuven.be)

vocabulary {
    type size := {0..9}
    gen1: size * size -> Bool
    gen2: size * size -> Bool 
    num_alive: size * size -> Int
    neighbours: size * size * size * size -> Bool
    total_alive: -> Int
}

theory {
    // Guarantee a grid size of 3x3 -- the still life shouldn't "spill out".
    !x in size: ~gen1(x, 0) & ~gen1(x, 9) & ~gen1(0, x) & ~gen1(9, x).
    !x in size: ~gen2(x, 0) & ~gen2(x, 9) & ~gen2(0, x) & ~gen2(9, x).

    // Count the number of living cells in gen1 for each cell.
    !x, y in size: num_alive(x, y) = #{x1, y1 in size: gen1(x1, y1) & -1 =< abs(x1 - x) =< 1 & -1 =< abs(y1 - y) =< 1 & ~(x = x1 & y = y1)}.

    // Express when a cell is alive in gen2 (i.e., the rules of Life).
    !x, y in size: gen2(x, y) <=> (gen1(x, y) & num_alive(x, y) in {2, 3}) | (~gen1(x, y) & num_alive(x, y) = 3).

    // Cells in gen1 should be the same as cells in gen2.
    !x, y in size: gen1(x, y) <=> gen2(x, y).
    
    total_alive() = #{x, y in size: gen1(x, y)}.
}

procedure main() {
    //pretty_print(model_expand(T))
    pretty_print(maximize(T,term="total_alive()"))
}
