def gen_graph_triangle(size: int) -> str:
    lp = f'node(0..{size-1}).\n'
    edges = [(i, j) for i in range(0,size) for j in [i+1,i+2,i+3] if j<size]
    for x, y in edges:
        lp += f'edge({x}, {y}). '
    lp += '\n\nphi(X, Y, Z) :- edge(X, Y), edge(Y, Z), edge(X, Z).'
    return lp


# for i in range(0, 2500, 200):
#     with open(f'graph_triangle_{i}.lp', 'w') as fp:
#         fp.write(gen_graph_triangle(i))

for i in range(10, 80, 10):
    with open(f'triangle_graph_{i}.lp', 'w') as fp:
        fp.write(gen_graph_triangle(i))

for i in [10, 100, 1000,  10000, 100000, 1000000]:
    with open(f'large_triangle_graph_{i}.lp', 'w') as fp:
        fp.write(gen_graph_triangle(i))
