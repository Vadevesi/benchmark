import json
import time
import subprocess
import resource
import datetime
from collections import defaultdict

IGNORE = [
        'ASP',
        'FIZ',
        'FOLASP',
        'IDP3',
        'IDP-Z3',
        'ManyWorlds',
        'SLI',
        'Z3Py',

        'Edges',
        'Miss Manners',
        'Miss Manners Fixed Gender',
        'Sudoku',
        'NQueens',
        'Pigeons',
        'Graceful',
        'Big Interpretation',
        'Half Interpretation',
        'Laptop',
        'Edges Propagation',
        'Stu-Reject Propagation',
        # 'Small Triangle Graph',
        # 'Large Triangle Graph',
        ]

def run_IDP_Z3(exe, file, size=None, timeout=None):
    path = f'IDP-Z3/{file}_{size}.idp'
    proc = subprocess.run([exe, path], capture_output=True,
                          timeout=timeout, check=True)
    print(proc.stderr)


def run_IDP3(exe, file, size=None, timeout=None):
    path = f'IDP3/{file}_{size}.idp'
    proc = subprocess.run([exe, path], capture_output=True,
                          timeout=timeout, check=True)
    print(proc.stderr)


def run_clingo(exe, file, size=None, timeout=None):
    path = f'ASP/{file}_{size}.lp'
    if '_prop' in file:
        # Propagation
        args = [exe, '-e', 'cautious', path]
    else:
        # MX
        args = [exe, path]

    proc = subprocess.run(args,
                          capture_output=True,
                          timeout=timeout)
    print(proc.stderr)


def run_FOLASP(exe, file, size=None, timeout=None):
    path = f'FOLASP/{file}_{size}.idp'
    exe = exe.split(' ')
    proc = subprocess.run([exe[0], exe[1], '-i', path, '-x', '-o', 'delme.txt'],
                          capture_output=True,
                          timeout=timeout,
                          check=True
                          )
    print(proc.stderr)


def run_FIZ(exe, file, size=None, timeout=None):
    path = f'FIZ/{file}_{size}.idp'
    exe = exe.split(' ')
    if '_prop' in file:
        # Propagation
        args = [exe[0], exe[1], path, 'propagate']
    else:
        # MX
        args = [exe[0], exe[1], path, 'expand']
    proc = subprocess.run(args,
                          capture_output=True,
                          timeout=timeout,
                          check=True
                          )
    print(proc.stderr)


def run_Z3Py(exe, file, size=None, timeout=None):
    path = f'Z3Py/{file}_{size}.py'
    proc = subprocess.run([exe, path],
                          capture_output=True,
                          timeout=timeout,
                          check=True
                          )
    print(proc.stderr)

def run_ManyWorlds(exe, file, size=None, timeout=None):
    path = f'ManyWorlds/{file}_{size}.mw'
    if '_prop' in file:
        # Propagation
        args = [exe, 'intersect', '--blockers=none', '--debug=0', '--deterministic=0', path]
    else:
        # MX
        args = [exe, 'find', '--blockers=none', '--debug=0', '--deterministic=0', path]
    proc = subprocess.run(args,
                          capture_output=True,
                          timeout=timeout,
                          check=True
                          )
    print(proc.stderr)

def run_SLI(exe, file, size=None, timeout=None):
    path = f'SLI/{file}_{size}.idp'
    if '_prop' in file:
        # Propagation
        raise NotImplementedError
    else:
        # MX
        args = [exe, path]
    proc = subprocess.run(args,
                          capture_output=True,
                          timeout=timeout,
                          check=True
                          )
    print(proc.stderr)


def run_benchmark(benchmark, exe, file, iterations, timeout):
    run_times = {}
    for size in sizes:
        run_time = 0.0
        try:
            for i in range(iterations):
                print(f"{benchmark}: {file}, {size}, {i}")
                start = time.time()
                benchmark(exe, file, size, timeout)
                run_time += time.time() - start
            run_time = run_time / iterations
        except subprocess.TimeoutExpired:
            run_time = '-1'
        except (MemoryError, UnboundLocalError):
            print(exe, 'memoryout')
            run_time = '-1'
        except subprocess.CalledProcessError:
            # breakpoint()
            print(exe, 'error?')
            run_time = '-1'
        run_times[size] = run_time
    return run_times

def write_json(dictionary):
    with open('./Site/timings.json', 'w') as fp:
        fp.write(json.dumps(dictionary, indent='\t'))

runners = {'ASP': run_clingo,
           'FIZ': run_FIZ,
           'FOLASP': run_FOLASP,
           'IDP3': run_IDP3,
           'IDP-Z3': run_IDP_Z3,
           'ManyWorlds': run_ManyWorlds,
           'Z3Py': run_Z3Py,
           'SLI': run_SLI}

with open('./benchmark.json', 'r') as fp:
    settings = json.loads(fp.read())

try:
    with open('./Site/timings.json', 'r') as fp:
        output = json.loads(fp.read())
    settings['timings'] = output['timings']
except FileNotFoundError:
    # Used to save the results to.
    settings['timings'] = {}

settings['date'] = datetime.datetime.now().strftime("%I:%M%p on %B %d, %Y")

# Set max ram usage.
soft, hard = resource.getrlimit(resource.RLIMIT_AS)
resource.setrlimit(resource.RLIMIT_AS,
                   (1024*1024*1024*settings['max_ram_GB'], hard))

iterations = settings['iterations']
timeout = settings['timeout_seconds']
for benchmark in settings['benchmarks']:
    if benchmark['Name'] in IGNORE:
        continue
    sizes = benchmark['Sizes']

    if benchmark['Name'] not in settings['timings']:
        timings = defaultdict(dict)
    else:
        timings = settings['timings'][benchmark['Name']]
    for engine in ['ASP', 'FIZ', 'FOLASP', 'IDP3', 'IDP-Z3', 'ManyWorlds',
                   'Z3Py', 'SLI']:
        if engine in IGNORE:
            continue
        for file in benchmark[engine]:
            runner = runners[engine]
            exe = settings['executables'][engine]
            times = run_benchmark(runner, exe, file, iterations, timeout)
            try:
                timings[engine][file] = times
            except KeyError:
                timings[engine] = {}
                timings[engine][file] = times
            # Save partial data as well.
            write_json(settings)
    settings['timings'][benchmark['Name']] = timings
    # Save partial data as well.
    write_json(settings)
