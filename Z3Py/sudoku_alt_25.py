#!/usr/bin/python -u
# -*- coding: latin-1 -*-
# 
# Sudoku in Z3.
#
# Here are some experiments.
#
# SolverFor("QF_LIA")
# Here are the times for first solution (i.e. prove_unicity = False):
#  - world_hardest (9x9):   0.178s
#  - another (9x9)      :   0.05s
#  - problem_34 (16x16) :   0.297s
#  - problem_89 (25x25) : 166.05s
#
# Here are the times for prove_unicity = True:
#  - world_hardest (9x9):   0.368s
#  - another (9x9)      :   0.093s
#  - problem_34 (16x16) :   0.69s
#  - problem_89 (25x25) : 465.17s
#
#
# QF_FD is slower than QF_LIA for finding first solution
# but faster for proving unicity.
#
# Time to first solution:
#  - world_hardest (9x9):   0.10s
#  - another (9x9)      :   0.049s
#  - problem_34 (16x16) :   0.338s
#  - problem_89 (25x25) : 205.10s
#
# Proving unicity:
#  - world_hardest (9x9):   0.136s
#  - another (9x9)      :   0.06s
#  - problem_34 (16x16) :   0.439s
#  - problem_89 (25x25) : 318.21s
#
#
# But using Tactics is significantly faster:
# 
# Using tactics: simplify, qffd:
# Time to first solution:
#  - world_hardest (9x9):   0.063s
#  - another (9x9)      :   0.049s
#  - problem_34 (16x16) :   0.29s
#  - problem_89 (25x25) :  16.46s
#
# Proving unicity:
#  - world_hardest (9x9):   0.115s
#  - another (9x9)      :   0.091s
#  - problem_34 (16x16) :   0.524s
#  - problem_89 (25x25) :  42.957s
#

#
# Compare with sudoku_ip.py that uses (pseuso) boolean with AtLeast and AtMost constraints
# which is slower on easy cases (9x9 and 16x16) but significantly faster on the 25x25 case:
# It solves problem 89 (25x25) in 3.87s for first solution and 5,49s proving unicity.
# 

# This Z3 model was written by Hakan Kjellerstrand (hakank@gmail.com)
# See also my Z3 page: http://hakank.org/z3/
#
# Slightly modified to fit our benchmark.

import time
from z3_utils_hakank import *

def sudoku(init,m=3,prove_unicity=True):
    
    # sol = Solver()
    # sol = SimpleSolver()
    # sol = SolverFor("QF_LIA")
    # if prove_unicity:
    #     sol = SolverFor("QF_FD")
    # else:
    #     sol = SolverFor("LIA")

    t1 = Tactic('simplify')
    # t2 = Tactic('pqffd') # slower and yieling weird results
    t2 = Tactic('qffd') 
    # t2 = Tactic('smtfd') # strange results
    sol  = Then(t1, t2).solver()

    
    n = m ** 2
    line = list(range(0, n))
    cell = list(range(0, m))

    # Setup
    x = {}
    for i in line: 
        for j in line:
            x[(i,j)] = Int('x %i %i' % (i,j))
            sol.add(x[(i,j)] >= 1, x[(i,j)] <= m*m)

    # Distinct on rows and columns
    for i in line:
        sol.add(Distinct([x[(i,j)] for j in line]))
        sol.add(Distinct([x[(j,i)] for j in line]))
    
    # Distinct on cells.
    for i in cell:
        for j in cell:
            this_cell = []
            for di in cell:
                for dj in cell:
                    this_cell.append(x[(i * m + di, j * m + dj)])
            sol.add(Distinct(this_cell))

    # Init grid
    for i in line:
        for j in line:
            if init[i][j]:
                sol.add(x[(i, j)] == init[i][j])

    # Check for unicity
    # (for just the first solution: change "while" to "if")
    if prove_unicity:
        while sol.check() == sat:
            mod = sol.model()
            print_grid(mod,x,n,n)
            print()
            getDifferentSolutionMatrix(sol,mod,x, n,n)
    else:
        if sol.check() == sat:
            mod = sol.model()
            print_grid(mod,x,n,n)

empty_row = [0] * 25
empty = [empty_row] * 25

sudoku(empty, 5, prove_unicity=False)
