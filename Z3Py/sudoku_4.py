# Sudoku implementation in Z3Py
# Author: Eric Pony
# Taken from https://ericpony.github.io/z3py-tutorial/guide-examples.htm
# Slightly modified to better fit the benchmark.

from z3 import Int, And, Distinct, If, Solver, sat

SIZE = 4
ROOT = 2

# 9x9 matrix of integer variables
X = [ [ Int("x_%s_%s" % (i+1, j+1)) for j in range(SIZE) ]
      for i in range(SIZE) ]

# each cell contains a value in {1, ..., 9}
cells_c  = [ And(1 <= X[i][j], X[i][j] <= SIZE)
             for i in range(SIZE) for j in range(SIZE) ]

# each row contains a digit at most once
rows_c   = [ Distinct(X[i]) for i in range(SIZE) ]

# each column contains a digit at most once
cols_c   = [ Distinct([ X[i][j] for i in range(SIZE) ])
             for j in range(SIZE) ]

# each 3x3 square contains a digit at most once
sq_c     = [ Distinct([ X[2*i0 + i][2*j0 + j]
                        for i in range(2) for j in range(2) ])
             for i0 in range(2) for j0 in range(2) ]

sudoku_c = cells_c + rows_c + cols_c + sq_c

# sudoku instance, we use '0' for empty cells
empty_row = [0] * SIZE
instance = [empty_row] * SIZE
# instance = ((0,0,0,0,0,0,0,0,0),
#             (0,0,0,0,0,0,0,0,0),
#             (0,0,0,0,0,0,0,0,0),
#             (0,0,0,0,0,0,0,0,0),
#             (0,0,0,0,0,0,0,0,0),
#             (0,0,0,0,0,0,0,0,0),
#             (0,0,0,0,0,0,0,0,0),
#             (0,0,0,0,0,0,0,0,0),
#             (0,0,0,0,0,0,0,0,0))

instance_c = [ If(instance[i][j] == 0,
                  True,
                  X[i][j] == instance[i][j])
               for i in range(SIZE) for j in range(SIZE) ]

s = Solver()
s.add(sudoku_c + instance_c)
if s.check() == sat:
    m = s.model()
    r = [ [ m.evaluate(X[i][j]) for j in range(SIZE) ]
          for i in range(SIZE) ]
    print(r)
else:
    print ("failed to solve")
