from z3 import Bool, Solver, Or, Not, And, Datatype, Function, BoolSort, Not


gender = Datatype('Gender')
gender.declare('f')
gender.declare('m')
Gender = gender.create()

person = Datatype('Person')
for i in range(16):
    person.declare(f'p{i}')
Person = person.create()

has_gender = Function('has_gender', Person, Gender)

solver = Solver()
breakpoint()

solver.check()
print(solver.model())
