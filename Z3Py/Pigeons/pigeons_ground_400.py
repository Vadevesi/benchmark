from z3 import Bool, And, Or, Solver, Const, IntSort

RANGE = 400

solver = Solver()
for i in range(0, RANGE):
    pigeon = Const(f'p{i}', IntSort())

    # Every pigeon gets a hole.
    solver.add(And(0 <= pigeon, pigeon < RANGE))
    for j in range(0, RANGE):
        if i == j:
            continue

        # No pigeons share the same hole.
        pigeon2 = Const(f'p{j}', IntSort())
        solver.add(pigeon2 != pigeon)

solver.check()
print(solver.model())
