from z3 import Bool, And, Or, Solver, Const, IntSort, Distinct

RANGE = 300

solver = Solver()
pigeons = []
for i in range(0, RANGE):
    pigeon = Const(f'p{i}', IntSort())
    # Every pigeon gets a hole.
    solver.add(And(0 <= pigeon, pigeon < RANGE))

    pigeons.append(pigeon)

solver.add(Distinct(pigeons))

solver.check()
print(solver.model())
