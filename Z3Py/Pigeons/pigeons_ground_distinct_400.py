from z3 import Bool, And, Or, Solver, Const, IntSort, Distinct

RANGE = 400

solver = Solver()
pigeons = []
for i in range(0, RANGE):
    pigeon = Const(f'p{i}', IntSort())
    # Every pigeon gets a hole.
    solver.add(And(0 <= pigeon, pigeon < RANGE))

    pigeons.append(pigeon)

solver.add(Distinct(pigeons))
#     for j in range(0, RANGE):
#         if i == j:
#             continue
# 
#         # No pigeons share the same hole.
#         pigeon2 = Const(f'p{j}', IntSort())
#         solver.add(pigeon2 != pigeon)

solver.check()
print(solver.model())
