from z3 import (Bool, Solver, Or, Not, And, Datatype, Function, BoolSort, Not,
                IntSort, ForAll, Implies, Int, Exists)

RANGE = 150

edge = Function('edge', IntSort(), IntSort(), BoolSort())
solver = Solver()

# for i in range(0, RANGE):
#     funcs = []  # for the existential constraint
#     for j in range(0, RANGE):
#         if i != j:
#             solver.add(Or(f(i, j), f(j, i)))
#             solver.add(Or(Not(f(i, j)), Not(f(j, i))))
# 
#             funcs.append(f(i, j))
#     solver.add(Or(funcs))

x = Int('x')
y = Int('y')


#  !x, y: x ~= y => (edge(x, y) | edge(y, x)).
solver.add(ForAll([x, y],
            Implies(
                And(x != y, 0 <= x, x < RANGE, 0 <= y, y < RANGE),
                Or(edge(x, y), edge(y, x))
                )
            ))

#  !x, y: x ~= y => (~edge(x, y) | ~edge(y, x)).
solver.add(ForAll([x, y],
            Implies(
                And(x != y, 0 <= x, x < RANGE, 0 <= y, y < RANGE),
                Or(Not(edge(x, y)), Not(edge(y, x)))
                )
            ))

solver.add(ForAll([x],
            Exists([y],
                Implies(
                    And(x != y, 0 <= x, x < RANGE, 0 <= y, y < RANGE),
                    edge(x, y))
                    )
              )
            )

solver.check()
print(solver.model())
