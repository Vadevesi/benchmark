from z3 import Bool, Solver, Or, Not, And, Datatype, Function, BoolSort, Not

RANGE = 150

node = Datatype('Node')
for i in range(0, RANGE):
    node.declare(f'_{i}')
Node = node.create()

f = Function('edge', Node, Node, BoolSort())
solver = Solver()

for i in range(0, RANGE):
    funcs = []  # for the existential constraint
    for j in range(0, RANGE):
        if i != j:
            b1 = getattr(Node, f'_{i}')
            b2 = getattr(Node, f'_{j}')
            solver.add(Or(f(b1, b2), f(b2, b1)))
            solver.add(Or(Not(f(b1, b2)), Not(f(b2, b1))))

            funcs.append(f(b1, b2))
    solver.add(Or(funcs))


solver.check()
print(solver.model())
