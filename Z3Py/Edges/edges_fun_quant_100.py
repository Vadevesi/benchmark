from z3 import (Bool, Solver, Or, Not, And, Datatype, Function, BoolSort, Not,
                IntSort, ForAll, Implies, Int, Exists, Const)

RANGE = 100

node = Datatype('Node')
for i in range(0, RANGE):
    node.declare(f'_{i}')
Node = node.create()

edge = Function('edge', Node, Node, BoolSort())
solver = Solver()

# for i in range(0, RANGE):
#     funcs = []  # for the existential constraint
#     for j in range(0, RANGE):
#         if i != j:
#             solver.add(Or(f(i, j), f(j, i)))
#             solver.add(Or(Not(f(i, j)), Not(f(j, i))))
# 
#             funcs.append(f(i, j))
#     solver.add(Or(funcs))

x = Const('x', Node)
y = Const('y', Node)


#  !x, y: x ~= y => (edge(x, y) | edge(y, x)).
solver.add(ForAll([x, y],
            Implies(
                x != y,
                Or(edge(x, y), edge(y, x))
                )
            ))

#  !x, y: x ~= y => (~edge(x, y) | ~edge(y, x)).
solver.add(ForAll([x, y],
            Implies(
                x != y,
                Or(Not(edge(x, y)), Not(edge(y, x)))
                )
            ))

solver.add(ForAll([x],
            Exists([y],
                Implies(
                    x != y,
                    edge(x, y))
                    )
              )
            )

solver.check()
print(solver.model())
