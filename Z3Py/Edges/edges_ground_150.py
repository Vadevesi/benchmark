from z3 import Bool, Solver, Or, Not, And


solver = Solver()

for i in range(0, 150):
    for j in range(0, 150):
        if i != j:
            b1 = Bool(f'edge-{i}-{j}')
            b2 = Bool(f'edge-{j}-{i}')
            solver.add(Or(b1, b2))
            solver.add(Not(And(b1, b2)))


solver.check()
print(solver.model())
