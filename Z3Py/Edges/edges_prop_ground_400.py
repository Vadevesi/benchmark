from z3 import Bool, Solver, Or, Not, And


solver = Solver()

symbols = []
for i in range(0, 400):
    for j in range(0, 400):
        if i != j:
            b1 = Bool(f'edge-{i}-{j}')
            b2 = Bool(f'edge-{j}-{i}')
            solver.add(Or(b1, b2))
            solver.add(Not(And(b1, b2)))

            symbols.append(b1)
            symbols.append(b2)
            if i == j + 10:
                solver.add(b2)


solver.check()
print(solver.consequences([], symbols))
