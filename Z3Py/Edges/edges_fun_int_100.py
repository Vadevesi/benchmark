from z3 import (Bool, Solver, Or, Not, And, Datatype, Function, BoolSort, Not,
                IntSort, ForAll, Implies, Int)

RANGE = 100

f = Function('edge', IntSort(), IntSort(), BoolSort())
solver = Solver()

for i in range(0, RANGE):
    funcs = []  # for the existential constraint
    for j in range(0, RANGE):
        if i != j:
            solver.add(Or(f(i, j), f(j, i)))
            solver.add(Or(Not(f(i, j)), Not(f(j, i))))

            funcs.append(f(i, j))
    solver.add(Or(funcs))

# Ensuring the domains is not necessary in this case.
# x = Int('x')
# y = Int('y')
# solver.add(ForAll([x, y], Implies(Or(x < 0, x >= RANGE, y < 0, y >= RANGE), Not(f(x, y)))))

solver.check()
print(solver.model())
