//
// Author: Pierre Carbonnelle (pierre.carbonnelle@kuleuven.be)

vocabulary V {
    // we use an extended square to simplify computations:
    // we add a 1-cell-wide border without mine

    type Range isa int
    type Count isa int
    max_val: Range
    type PseudoBol isa int
    has_mine(Range, Range): PseudoBol
    count(Range, Range): Count
    clicked(Range, Range)
    shown(Range, Range)
}

theory T:V {
    ?c[Range]: max_val = c.
    !c[Range]: c =< max_val.

    // borders don't have mine
    !c[Range]: has_mine(c,0    )=0 & has_mine(0    ,c)=0
               & has_mine(c,max_val)=0 & has_mine(max_val,c)=0.

    // computation of count
    !r[Range],c[Range]: r =< 0 | r >= max_val | c =< 0 | c >= max_val => count(r,c) = 0.
    !r[Range],c[Range]: ~(r =< 0 | r >= max_val | c =< 0 | c >= max_val) => count(r,c) = 
            ( has_mine(r,c)
            + has_mine(r-1, c-1) + has_mine(r-1, c  ) + has_mine(r-1, c+1)
            + has_mine(r+1, c-1) + has_mine(r+1, c  ) + has_mine(r+1, c+1)
            + has_mine(r, c-1) + has_mine(r, c+1)).

    // what is shown is determined by clicked
    { 
        ! r[Range], c[Range]: shown(r,c) <- ~(r=<0 | r>=max_val() | c=<0 | c>=max_val()) & clicked(r,c).
        ! r[Range], c[Range]: shown(r,c) <- ~(r=<0 | r>=max_val() | c=<0 | c>=max_val())  // but borders are not shown
                                            & ( (shown(r-1,c-1) & count(r-1,c-1)=0)
                                              | (shown(r-1,c+1) & count(r-1,c+1)=0)
                                              | (shown(r+1,c-1) & count(r+1,c-1)=0)
                                              | (shown(r+1,c+1) & count(r+1,c+1)=0)
                                              | (shown(r-1,c  ) & count(r-1,c  )=0)
                                              | (shown(r+1,c  ) & count(r+1,c  )=0)
                                              | (shown(r  ,c-1) & count(r  ,c-1)=0)
                                              | (shown(r  ,c+1) & count(r  ,c+1)=0)
                                              ).
    }
}

structure S:V {
    Count = {0..8}
    Range = {0..31}
    PseudoBol = {0;1}
}

procedure main() {
    print(optimalpropagate(T,S))
}
