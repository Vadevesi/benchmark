vocabulary = """
vocabulary V {{
    type node = {{0..{}}}
    phi(node, node, node)
    edge(node, node)
}}
"""

theory = """
theory T : V {
    !x[node]: !y[node]: !z[node]: phi(x, y, z) <=> edge(x, y) & edge(y, z) & edge(x, z).
}
"""

structure = """
structure S : V {{
    edge = {{{}}}
}}
"""

main = """
procedure main() {
    stdoptions.nbmodels = 1
    printmodels(modelexpand(T,S))
}
"""


def gen_triangle_graph(size):
    spec = vocabulary.format(size)
    edges = [f"({i}, {j})" for i in range(0,size) for j in [i+1,i+2,i+3] if j<size]
    spec += theory
    spec += structure.format("; ".join(edges))
    spec += main
    return spec

for i in range(10, 80, 10):
    spec = gen_triangle_graph(i)
    with open(f'triangle_graph_{i}.idp', 'w') as fp:
        fp.write(spec)

for i in [10, 100, 1000, 10000, 100000, 1000000]:
    spec = gen_triangle_graph(i)
    with open(f'large_triangle_graph_{i}.idp', 'w') as fp:
        fp.write(spec)

