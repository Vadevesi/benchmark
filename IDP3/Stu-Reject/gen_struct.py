# Generates a structure of variable size for the IDP-Z3 propagation benchmark.
# Also generates the ASP equivalent.

import random

random.seed(2023)
for size in range(10, 110, 10):

    struct = 'structure S: V{\n'
    struct += f'Type1 = {{1..{size}}}\n'
    struct += f'Type2 = {{1..{size*5}}}\n'
    struct += f'mean = {size//2}\n'

    function_vals = []
    for i in range(1, size*5+1):
        choice = random.randint(1, size)
        function_vals.append(f'({i}, {choice})')
    struct += f"function1 = {{{'; '.join(function_vals)}}}\n"
    struct += '}\n'

    with open('./stureject_theory.idp', 'r') as fp:
        theory = fp.read()
    with open(f'stureject_prop_{size}.idp', 'w') as fp:
        fp.write(theory)
        fp.write(struct)
        fp.write('procedure main() { print(optimalpropagate(T,S))}')
