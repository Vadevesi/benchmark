// set the dimensions and margins of the graph
screen_width = window.screen.width
screen_height = window.screen.height
const margin = {top: 10, right: screen_width*0.25, bottom: 30, left: screen_width*0.25},
    width = window.screen.width - margin.left - margin.right,
    height = window.screen.height*0.6 - margin.top - margin.bottom;

window.screen.height;
window.screen.width;


// Based on https://stackoverflow.com/a/66494926
function getColor(stringInput) {
    let stringUniqueHash = [...stringInput].reduce((acc, char) => {
        return char.charCodeAt(0) + ((acc << 5) - acc);
    }, 0);
    return `hsl(${stringUniqueHash % 360}, 100%, 45%)`;
}


let json_data = null
function draw_graph(json) {
// append the svg object to the body of the page
   const svg = d3.select("#my_dataviz")
     .append("svg")
       .attr("width", width + margin.left + margin.right)
       .attr("height", height + margin.top + margin.bottom)
     .append("g")
       .attr("transform",`translate(${margin.left},${margin.top})`);
       
    let problem = document.getElementById('problemSelector').value
    if (!problem) {
        problem = 'Edges';
    }
    timings = json['timings'][problem]  // hard code edges for now
    const allEngines = Object.keys(timings)


    // Gather a list containing the names of the lines to draw.
    // We also prepare the data: {name: ..., values: [{size, time}, ...]}
    const allGroup = []
    const allData = []
    const allColor = {}
    let max_time = 0
    for (i in allEngines) {
      for (j in timings[allEngines[i]]) {
        name = allEngines[i] + ';' + j
        if (document.getElementById('checkbox_' + name) && !document.getElementById('checkbox_' + name).checked) {
          continue
        }
        allGroup.push(name);

        allColor[name] = getColor(name);

        const data = {};
        data['name'] = name;
        data['values'] = [];
        for (size in timings[allEngines[i]][j]) {
            const time = timings[allEngines[i]][j][size]
            if (time <= 0) {
              // Timeout or memory-out
              continue
            }
            data['values'].push({'size': size, 'time': time});
            if (time > max_time) {
              max_time = time;
            }
        }
        if (data['values'].length == 0) {
          // Avoid pushing empty data
          continue
        }
        allData.push(data)
      }
    }
    // Very messy way to find y-values, but it works. Basically, grab the y-values of the first engine.
    const yValues = Object.keys(Object.values(timings[Object.keys(timings)[1]])[0])
    min_size = parseInt(yValues[0])
    max_size = parseInt(yValues[yValues.length-1])
    console.log(min_size, max_size)
    console.log(allData)

    // Create the graph.
    // See also https://d3-graph-gallery.com/graph/connectedscatter_multi.html
    // A color scale: one color for each group
    //const myColor = d3.scaleOrdinal()
    //  .domain(allGroup)
    //  .range(d3.schemeSet2);
    //console.log(myColor)

    // Add X axis
    let x = null
    if (document.getElementById('xScaleType').checked) {
      x = d3.scaleLog()
      .domain([min_size*0.90,max_size*1.1])
      .range([ 0, width ]);
    svg.append("g")
      .attr("transform", `translate(0, ${height})`)
      .call(d3.axisBottom(x));
    } else {
      x = d3.scaleLinear()
       .domain([min_size*0.90,max_size*1.1])
       .range([ 0, width ]);
     svg.append("g")
       .attr("transform", `translate(0, ${height})`)
       .call(d3.axisBottom(x));
    }

    // Add Y axis
    let y = null
    if (document.getElementById('yScaleType').checked) {
      y = d3.scaleLog()
      .domain( [0.001,max_time*1.05])
      .range([ height, 0 ]);
    svg.append("g")
      .call(d3.axisLeft(y));
    } else {
      y = d3.scaleLinear()
      .domain( [0.001,max_time*1.05])
      .range([ height, 0 ]);
    svg.append("g")
      .call(d3.axisLeft(y));
    }

    // Add the lines
    const line = d3.line()
      .x(d => x(+d.size))
      .y(d => y(+d.time))
    svg.selectAll("myLines")
      .data(allData)
      .join("path")
        .attr("d", d => line(d.values))
        .attr("stroke", d => allColor[d.name])
        .style("stroke-width", 4)
        .style("fill", "none")

    // Define a cool hover tooltip
    // create a tooltip
    const tooltip = d3.select("#my_dataviz")
    .append("div")
    .style("opacity", 0)
    .attr("class", "tooltip")
    .style("color", "white")
    .style('position', 'absolute')

    // Three function that change the tooltip when user hover / move / leave a cell
    let mouseover = function(event, d) {
      tooltip
        .style("opacity", 1)
    }
    let mousemove = function(event, d) {
      tooltip
      .html(`${d.time.toFixed(3)}s`)
      .style("left", (event.pageX-50) + "px")
      .style("top", (event.pageY-20) + "px")
      console.log((d))
      console.log(screen.mouseX)

    }
    let mouseleave = function(event, d) {
      tooltip
        .style("opacity", 0)
    }


    // Add the points
    svg
      // First we need to enter in a group
      .selectAll("myDots")
      .data(allData)
      .join('g')
        .style("fill", d => allColor[d.name])
      // Second we need to enter in the 'values' part of this group
      .selectAll("myPoints")
      .data(d => d.values)
      .join("circle")
        .attr("cx", d => x(d.size))
        .attr("cy", d => y(d.time))
        .attr("r", 5)
        .attr("stroke", "white")
      .on("mouseover", mouseover)
      .on("mousemove", mousemove)
      .on("mouseleave", mouseleave)

    // Add a legend at the end of each line
    svg
      .selectAll("myLabels")
      .data(allData)
      .join('g')
        .append("text")
          .datum(d => { console.log(d); return {name: d.name, value: d.values[d.values.length - 1]}; }) // keep only the last value of each time series
          .attr("transform",d => `translate(${x(d.value.size)},${y(d.value.time)})`) // Put the text at the position of the last point
          .attr("x", 12) // shift the text a bit more right
          .text(d => d.name)
          .style("fill", d => allColor[d.name])
          .style("font-size", 15)
};

function redraw_graph() {
  document.getElementById('my_dataviz').innerHTML = '';
  draw_graph(json_data)

  // Also update the check boxes.
}

function update_checkboxes() {
  // Reset current boxes.
  document.getElementById('boxes').innerHTML = '';

  // Gather a list containing the data sources
  let problem = document.getElementById('problemSelector').value
  if (!problem) {
      problem = 'Edges';
  }
  timings = json_data['timings'][problem]  // hard code edges for now
  const allEngines = Object.keys(timings)

  const allGroup = []
  let max_time = 0
  for (i in allEngines) {
    for (j in timings[allEngines[i]]) {
      name = allEngines[i] + ';' + j
      allGroup.push(name);
    }
  }

  // For each data source, introduce a check box.
  for (idx = 0; idx < allGroup.length; idx++) {
    const checkbox = document.createElement('input')
    checkbox.type = 'checkbox';
    checkbox.name = allGroup[idx];
    checkbox.id = 'checkbox_' + allGroup[idx];
    checkbox.checked = true;
    checkbox.addEventListener('click', function () { redraw_graph()});
    document.getElementById('boxes').appendChild(checkbox)

    const label = document.createElement('label')
    label.htmlFor = 'checkbox_' + allGroup[idx];
    label.innerHTML = allGroup[idx];
    document.getElementById('boxes').appendChild(label)
    


  }
  console.log(allGroup)
}

function init_UI(json) {
  json_data = json;
  console.log(json)
  // Make a drop-down to select a problem.
  for (idx = 0; idx < json_data["benchmarks"].length; idx++) {
    // Create an option element.
    const option = document.createElement('option');
    option.textContent = json_data["benchmarks"][idx].Name;
    document.getElementById('problemSelector').appendChild(option);
  }

  document.getElementById('memory').innerHTML = json_data['max_ram_GB']
  document.getElementById('timeout').innerHTML = json_data['timeout_seconds']

}

fetch('timings.json').then((response) => response.json()).then(function(json) { draw_graph(json); init_UI(json); update_checkboxes()});
