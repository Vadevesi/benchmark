// Implementation of Miss Manners challenge (https://dmcommunity.org/challenge/challenge-june-2023/)
// Author: Jo Devriendt (jo.devriendt@nonfictionsoftware.com)

decdef nSeats as 64.
decdef nHobbies as 3.

decdef Seat as {"s"1..nSeats()}.
decdef Person as {"p"1..nSeats()}.
decdef Hobby as {"h"1..nHobbies()}.

declare seat: Person -> Seat.
declare next: Seat -> Seat.
define next as {$python
def next():
 return [("s"+str(i+1),"s"+str((i+1)%64+1)) for i in range(0,64)]
$} default "s1".
decdef EvenSeat as {$python
def EvenSeat():
 return ["s"+str(2*i+2) for i in range(0,64//2)]
$}.

decdef woman as {"p2","p7","p12","p16","p17","p19","p20","p23","p24","p25","p27","p29","p30","p34","p35","p38","p40","p46","p48","p51","p53","p54","p55","p56","p57","p58","p59","p60","p61","p62","p63","p64"}.

decdef has_hobby as {("p1","h2"),("p1","h1"),("p1","h3"),("p2","h2"),("p2","h1"),("p2","h3"),("p3","h3"),("p3","h2"),("p4","h3"),("p4","h2"),("p4","h1"),("p5","h2"),("p5","h1"),("p5","h3"),("p6","h2"),("p6","h3"),("p6","h1"),("p7","h1"),("p7","h2"),("p7","h3"),("p8","h3"),("p8","h1"),("p9","h2"),("p9","h3"),("p9","h1"),("p10","h3"),("p10","h2"),("p10","h1"),("p11","h1"),("p11","h3"),("p11","h2"),("p12","h3"),("p12","h1"),("p12","h2"),("p13","h2"),("p13","h3"),("p14","h1"),("p14","h2"),("p15","h2"),("p15","h3"),("p15","h1"),("p16","h2"),("p16","h3"),("p17","h3"),("p17","h2"),("p18","h1"),("p18","h3"),("p18","h2"),("p19","h3"),("p19","h1"),("p20","h1"),("p20","h3"),("p20","h2"),("p21","h2"),("p21","h3"),("p22","h2"),("p22","h3"),("p23","h1"),("p23","h2"),("p24","h3"),("p24","h1"),("p24","h2"),("p25","h3"),("p25","h1"),("p25","h2"),("p26","h2"),("p26","h1"),("p26","h3"),("p27","h2"),("p27","h3"),("p27","h1"),("p28","h1"),("p28","h2"),("p29","h2"),("p29","h3"),("p29","h1"),("p30","h2"),("p30","h1"),("p30","h3"),("p31","h1"),("p31","h2"),("p31","h3"),("p32","h1"),("p32","h2"),("p33","h2"),("p33","h3"),("p33","h1"),("p34","h2"),("p34","h1"),("p34","h3"),("p35","h2"),("p35","h3"),("p36","h2"),("p36","h1"),("p37","h2"),("p37","h1"),("p38","h1"),("p38","h3"),("p38","h2"),("p39","h3"),("p39","h1"),("p39","h2"),("p40","h1"),("p40","h2"),("p40","h3"),("p41","h2"),("p41","h1"),("p41","h3"),("p42","h3"),("p42","h1"),("p43","h1"),("p43","h3"),("p43","h2"),("p44","h3"),("p44","h1"),("p44","h2"),("p45","h1"),("p45","h2"),("p46","h1"),("p46","h2"),("p46","h3"),("p47","h1"),("p47","h2"),("p48","h3"),("p48","h2"),("p49","h3"),("p49","h2"),("p50","h2"),("p50","h3"),("p51","h2"),("p51","h1"),("p51","h3"),("p52","h1"),("p52","h2"),("p52","h3"),("p53","h2"),("p53","h1"),("p54","h1"),("p54","h2"),("p54","h3"),("p55","h1"),("p55","h2"),("p55","h3"),("p56","h2"),("p56","h1"),("p56","h3"),("p57","h3"),("p57","h2"),("p57","h1"),("p58","h3"),("p58","h1"),("p58","h2"),("p59","h1"),("p59","h2"),("p59","h3"),("p60","h3"),("p60","h1"),("p61","h3"),("p61","h2"),("p61","h1"),("p62","h2"),("p62","h3"),("p63","h3"),("p63","h1"),("p63","h2"),("p64","h3"),("p64","h2")}.

// People cannot share a seat.
distinct [ seat(p) for p where Person(p) ].

// Adjacent people should have different gender.
all [ woman(p1) != woman(p2) for p1,p2 where Person(p1) and Person(p2) and p1!=p2 and next(seat(p1)) = seat(p2) ].

// Adjacent people should share at least one hobby.
all [ any [ has_hobby(p1, h) and has_hobby(p2, h) for h where Hobby(h) ] for p1, p2 where Person(p1) and Person(p2) and p1!=p2 and next(seat(p1)) = seat(p2) ].
