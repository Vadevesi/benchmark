// ManyWorlds implementation of the graceful graphs example
// https://www.mat.unical.it/aspcomp2013/GracefulGraphs
// Author: Jo Devriendt (jo.devriendt@nonfictionsoftware.com)

decdef Node as {1..N()}.
decdef NodeVal as {0..E()}.
decdef EdgeVal as {1..E()}.

declare node_value: Node, NodeVal -> bool.
declare edge_value: Node, Node, EdgeVal -> bool.

// The value of an edge is defined as the absolute difference of the values of the nodes
// TODO: below definition:
// define edge_value(x,y,abs(u-v)) as true where edge(x,y) and node_value(x,u) and node_value(y,v) else false.

all [ edge_value(x,y,abs(u-v)) for x,y,u,v where edge(x,y) and NodeVal(u) and NodeVal(v) and node_value(x,u) and node_value(y,v) and u != v].

// each node maps to one value
all [ count [ node_value(x,u) for u where NodeVal(u) ] = 1 for x where Node(x) ].

// all values of a node are unique
all [ count [ node_value(x,u) for x where Node(x) ] <= 1 for u where NodeVal(u) ].

// each edge maps to one value
all [ count [ edge_value(x,y,z) for z where EdgeVal(z) ] = 1 for x,y where edge(x,y) ].

// all values of an edge are unique
all [ count [ edge_value(x,y,z) for x,y where edge(x,y) ] <= 1 for z where EdgeVal(z) ].

decdef E as 35 .
decdef N as 11 .
decdef edge as  {(3, 4), (4, 9), (3, 10), (5, 10), (2, 11), (1, 9), (2, 8), (6, 11), (7, 10), (6, 8), (4, 5), (3, 9), (5, 6), (4, 8), (5, 9), (4, 11), (1, 2), (1, 11), (2, 10), (1, 8), (7, 9), (6, 7), (6, 10), (3, 11), (4, 10), (3, 8), (5, 11), (5, 8), (2, 3), (2, 9), (1, 7), (1, 10), (7, 11), (6, 9), (7, 8)} .
