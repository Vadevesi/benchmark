for theory in ["graceful.mw","graceful_alt.mw"]:
    common = ""
    with open(theory) as f:
        common = f.read()

    for instance in ["0001-quadruple_cone_5-25-1.asp","0002-triple_cone_6-24-1.asp","0003-quadruple_cone_6-30-1.asp","0004-double_cone_6-18-1.asp","0005-quadruple_cone_7-35-1.asp","0006-triple_cone_8-32-1.asp","0007-windmill_5_2-20-1.asp","0008-clique_path_5_2-25-1.asp","0009-clique_path_6_2-36-1.asp","0010-triple_cone_9-36-1.asp","0011-clique_path_8_2-64-1.asp","0051-prism_5_5-45-1.asp"]:
        data = ""
        with open(instance) as f:
            data = f.read()
        nums = [int(x) for x in data.replace(" ","").replace(")","").replace("(","").split(',') if x != '\n']
        edges = []
        for i in range(0,len(nums)//2):
            edges += [tuple(sorted([nums[2*i],nums[2*i+1]]))]
        edgeset = set(edges)
        assert(len(edges)==len(edgeset))
        outfile = theory[:-3]+"_"+instance[0:4]+".mw"
        with open(outfile, "w") as out:
            print(common,file=out)
            print("decdef E as",len(edgeset),".",file=out)
            print("decdef N as",max(nums),".",file=out)
            print("decdef edge as ",edgeset,".",file=out)



