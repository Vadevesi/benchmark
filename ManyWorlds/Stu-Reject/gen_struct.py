# Generates a structure of variable size for the ManyWorlds propagation benchmark.

import random

random.seed(2023)
for size in range(10, 110, 10):

    struct = f'decdef Type1 as {{1..{size}}}.\n'
    struct += f'decdef Type2 as {{1..{size*5}}}.\n'
    struct += f'decdef mean as {size//2}.\n'

    function_vals = []
    for i in range(1, size*5+1):
        choice = random.randint(1, size)
        function_vals.append(f'({i}, {choice})')
    struct += f"define function1 as {{{', '.join(function_vals)}}} default 1.\n"

    with open('./stureject_theory.mw', 'r') as fp:
        theory = fp.read()
    with open(f'stureject_prop_{size}.mw', 'w') as fp:
        fp.write(struct)
        fp.write(theory)
