// Implementation of Miss Manners challenge (https://dmcommunity.org/challenge/challenge-june-2023/)
// Author: Jo Devriendt (jo.devriendt@nonfictionsoftware.com)

decdef nSeats as 32.
decdef nHobbies as 3.

decdef Seat as {"s"1..nSeats()}.
decdef Person as {"p"1..nSeats()}.
decdef Hobby as {"h"1..nHobbies()}.

declare seat: Person -> Seat.
declare next: Seat -> Seat.
define next as {$python
def next():
 return [("s"+str(i+1),"s"+str((i+1)%32+1)) for i in range(0,32)]
$} default "s1".
decdef EvenSeat as {$python
def EvenSeat():
 return ["s"+str(2*i+2) for i in range(0,32//2)]
$}.

decdef woman as {"p3","p4","p6","p7","p10","p11","p13","p15","p19","p20","p22","p23","p26","p27","p29","p31"}.

decdef has_hobby as {("p1","h1"),("p1","h3"),("p1","h2"),("p2","h3"),("p2","h1"),("p2","h2"),("p3","h2"),("p3","h1"),("p4","h3"),("p4","h1"),("p5","h2"),("p5","h3"),("p6","h3"),("p6","h2"),("p7","h2"),("p7","h1"),("p7","h3"),("p8","h1"),("p8","h2"),("p9","h1"),("p9","h3"),("p10","h2"),("p10","h1"),("p10","h3"),("p11","h1"),("p11","h2"),("p12","h2"),("p12","h3"),("p13","h2"),("p13","h1"),("p13","h3"),("p14","h1"),("p14","h3"),("p15","h1"),("p15","h3"),("p16","h3"),("p16","h1"),("p17","h1"),("p17","h3"),("p17","h2"),("p18","h3"),("p18","h1"),("p18","h2"),("p19","h2"),("p19","h1"),("p20","h3"),("p20","h1"),("p21","h2"),("p21","h3"),("p22","h3"),("p22","h2"),("p23","h2"),("p23","h1"),("p23","h3"),("p24","h1"),("p24","h2"),("p25","h1"),("p25","h3"),("p26","h2"),("p26","h1"),("p26","h3"),("p27","h1"),("p27","h2"),("p28","h2"),("p28","h3"),("p29","h2"),("p29","h1"),("p29","h3"),("p30","h1"),("p30","h3"),("p31","h1"),("p31","h3"),("p32","h3"),("p32","h1")}.

// People cannot share a seat.
distinct [ seat(p) for p where Person(p) ].

// Women are placed on even seats, men on odd seats.
all [ woman(p) = EvenSeat(seat(p)) for p where Person(p) ].

// Adjacent people should share at least one hobby.
all [ any [ has_hobby(p1, h) and has_hobby(p2, h) for h where Hobby(h) ] for p1, p2 where Person(p1) and Person(p2) and p1!=p2 and next(seat(p1)) = seat(p2) ].
