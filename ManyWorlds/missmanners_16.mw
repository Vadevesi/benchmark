// Implementation of Miss Manners challenge (https://dmcommunity.org/challenge/challenge-june-2023/)
// Author: Jo Devriendt (jo.devriendt@nonfictionsoftware.com)

decdef nSeats as 16.
decdef nHobbies as 3.

decdef Seat as {"s"1..nSeats()}.
decdef Person as {"p"1..nSeats()}.
decdef Hobby as {"h"1..nHobbies()}.

declare seat: Person -> Seat.
declare next: Seat -> Seat.
define next as {$python
def next():
 return [("s"+str(i+1),"s"+str((i+1)%16+1)) for i in range(0,16)]
$} default "s1".
decdef EvenSeat as {$python
def EvenSeat():
 return ["s"+str(2*i+2) for i in range(0,16//2)]
$}.

decdef woman as {"p2","p7","p11","p12","p13","p14","p15","p16"}.

decdef has_hobby as {("p1","h2"),("p1","h1"),("p1","h3"),("p2","h2"),("p2","h1"),("p2","h3"),("p3","h3"),("p3","h2"),("p4","h3"),("p4","h2"),("p4","h1"),("p5","h2"),("p5","h1"),("p5","h3"),("p6","h2"),("p6","h3"),("p6","h1"),("p7","h1"),("p7","h2"),("p7","h3"),("p8","h3"),("p8","h1"),("p9","h2"),("p9","h3"),("p9","h1"),("p10","h3"),("p10","h2"),("p10","h1"),("p11","h1"),("p11","h3"),("p11","h2"),("p12","h3"),("p12","h1"),("p12","h2"),("p13","h2"),("p13","h3"),("p14","h1"),("p14","h2"),("p14","h3"),("p15","h2"),("p15","h3"),("p15","h1"),("p16","h2"),("p16","h3")}.

// People cannot share a seat.
distinct [ seat(p) for p where Person(p) ].

// Adjacent people should have different gender.
all [ woman(p1) != woman(p2) for p1,p2 where Person(p1) and Person(p2) and p1!=p2 and next(seat(p1)) = seat(p2) ].

// Adjacent people should share at least one hobby.
all [ any [ has_hobby(p1, h) and has_hobby(p2, h) for h where Hobby(h) ] for p1, p2 where Person(p1) and Person(p2) and p1!=p2 and next(seat(p1)) = seat(p2) ].
