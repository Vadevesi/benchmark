/* Efficient NQueens implementation
Author: Jo Devriendt (jo.devriendt@nonfictionsoftware.com) */

decdef M as 19.
decdef N as {1..M()}.
decdef D as {1-2*M()..2*M()-1}.

declare Queen: N, N -> bool.

// exactly M queens
count [ Queen(r,c) for r,c where N(r) and N(c) ] = M().
// exactly 1 queen on each row
all [ count [ Queen(r,c) for r where N(r) ] = 1 for c where N(c) ].
// exactly 1 queen on each column
all [ count [ Queen(r,c) for c where N(c) ] = 1 for r where N(r) ].
// at most 1 queen on each diagonal
all [ count [ Queen(r,c) for r,c where Diagonal(r,c,d) ] <= 1 for d where D(d) ].

// Python definition to link queens to diagonals
decdef Diagonal as {$python
def Diagonal():
  M = 19
  N = range(1,1+M)
  return [(r,c,r-c-M) for r in N for c in N] + [(r,c,c+r-1) for r in N for c in N]
$}.

