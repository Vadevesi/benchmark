# File to generate SMT encoding for sudoku.
# Author: Simon Vandevelde (s.vandevelde@kuleuven.be)

import sys
from collections import defaultdict
import math

if len(sys.argv) > 1:
    SIZE = int(sys.argv[1])
    N = math.sqrt(SIZE)
else:
    SIZE = 4
    N = 2


declarations = []
val_asserts = []

for i in range(1, SIZE+1):
    for j in range(1, SIZE+1):
        declarations.append(f"(declare-fun val-{i}-{j} () Int)")
        val_asserts.append(f"(assert (and (> val-{i}-{j} 0) (< val-{i}-{j} {SIZE+1})))")

row_cons = []
col_cons = []
box_cons = defaultdict(lambda: "(assert (distinct ")
for i in range(1, SIZE+1):
    row_con = "(assert (distinct "
    col_con = "(assert (distinct "
    for j in range(1, SIZE+1):
        # Append to row and col constraints.
        row_con += f"val-{i}-{j} "
        col_con += f"val-{j}-{i} "

        # Calculate to which box this cell would belong and append to correct
        # one.
        box_x = (i-1)//N
        box_y = (j-1)//N
        box_cons[f'{box_x}{box_y}'] += f"val-{i}-{j} "

    row_con += '))'
    row_cons.append(row_con)
    col_con += '))'
    col_cons.append(col_con)

for box in box_cons:
    box_cons[box] += '))'

print('; Declare all variables')
print('\n'.join(declarations))
print('\n; Restrict the values of the variables to [1..SIZE]')
print('\n'.join(val_asserts))
print('\n; Values should be distinct in rows')
print('\n'.join(row_cons))
print('\n; Values should be distinct in cols')
print('\n'.join(col_cons))
print('\n; Values should be distinct in boxes')
print('\n'.join(box_cons.values()))
print('\n(check-sat)\n(get-model)')
