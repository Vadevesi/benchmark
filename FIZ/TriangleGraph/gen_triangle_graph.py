vocabulary = """
vocabulary {{
    type node := {{0..{}}}
    phi: node * node * node -> Bool
    edge: node * node -> Bool
}}
"""

theory = """
theory {
    !x, y, z  in node: phi(x, y, z) <=> edge(x, y) & edge(y, z) & edge(x, z).
}
"""

structure = """
structure {{
    edge := {{{}}}.
}}
"""

main = """
procedure main() {
    pretty_print(Theory(T,S).expand(max=1, timeout_seconds=0))
}
"""

def gen_triangle_graph(size):
    spec = vocabulary.format(size)
    edges = [f"({i}, {j})" for i in range(0,size) for j in [i+1,i+2,i+3] if j<size]
    spec += theory
    spec += structure.format(", ".join(edges))
    spec += main
    return spec

for i in range(10, 80, 10):
    spec = gen_triangle_graph(i)
    with open(f'triangle_graph_{i}.idp', 'w') as fp:
        fp.write(spec)

for i in [10, 100, 1000, 10000, 100000, 1000000]:
    spec = gen_triangle_graph(i)
    with open(f'large_triangle_graph_{i}.idp', 'w') as fp:
        fp.write(spec)
